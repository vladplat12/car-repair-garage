/*Ilona Aliev 319252813 | Vladislav Platunov 323615385*/
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <semaphore.h>
#include <unistd.h>

typedef enum  BOOL { FALSE = 0, TRUE = 1 } BOOL;

/*---Structs declarations---*/

typedef struct Resource {
	int id;
	char* name;
	int quantity;
	struct Resource* prev;
	struct Resource* next;
	sem_t mutex_AvailableVsQuantity;
	/* take care that the change of values available/quantity 
	will made by at most 1 thread in the same time*/
	BOOL available; 
	/*if quantity bigger then 0 - true, if quantity equals to 0- false*/
}resource;
typedef struct Repair {
	int  id;
	char* name;
	int duration;
	int num_of_resources;
	resource** list_of_resources;
	struct Repair* prev;
	struct Repair* next;
}repair;
typedef struct Request {
	char carID[8];
	int arrival_time;
	int num_of_repairs;
	repair** list_of_repairs;
	struct Request* prev;
	struct Request* next;
	BOOL arrive;
	/*at start will init to be FALSE,
	if car arrived i.e request thread already created - value changed to be true*/
	sem_t sem_CarNotInRepair;
	/*at start will init to 1, i.e car not in repair so 1=true. 
	if it changes to be 0, so car in repair now*/
}request;

/*Struct that manages all data that comming from input files*/
typedef struct Manager {
	int numOfDiffrentReresources;
	int numOfRepairs;
	int numOfRequests;
	repair* headRepairs;
	resource* headResources;
	request** requests_in_time;
	/*it a kind of array in size 24 (will set latter), while in every cell "i" 
	there is list of request that arrival in time "i"*/
}manager;

/*Stuct that represents one repair that related to specific request*/
typedef struct CarInRepair {
	repair* currentRepair;
	request* currentRequest;
	BOOL repairCompleted;
	/*at start will init to be FALSE,
	if repair finished successfully - value changed to be true*/
}cir;

/*---Function and gloval variables declarations---*/

manager* mngr;

/*Matchs between file and appropriate struct according file name and call appropriate func 
that inserts all data from file to appropriate list of structs*/
void inputAnalayze(void(*build_func)(char*), char*, char**);

/*Function that inserts all data from "resources.txt" to "headResources" list of resource structs*/
void buildForResources(char*);

/*Function that inserts all data from "repairs.txt" to "headRepairs" list of repairs structs*/
void buildForRepairs(char*);
/*Auxiliary Method for buildForRepairs func*/
resource* search_resource(resource*, int);

/*Function that inserts all data from "requests.txt" to "headResources" list of request structs*/
void buildForRequests(char*);
/*Auxiliary Method for buildForRequests func*/
repair* search_repairs(repair*, int);
/*Auxiliary Method for buildForRequests func*/
void add_to_requests_in_time(request**, request*);

/*global variable that represent time by seconds*/
int timer;

/* take care that while timer var changes- no one can not check(read) timer*/
sem_t mutex_timer;

/*Function that used by timerThread only- 
increases timer and reset timer if new day begins*/
void timerManage();

/*Function that used by all RequestsThreads*/
void RequestInTreat(void*);
/*Function that used by all repairsPerRequestThreads*/
void RepairInTreat(void*);

/*void printAllStructs(); for checking that all data from files in structs*/

int main(int argc, char* argv[]) {
	if (argc != 4) { printf("Miss all files");   return 1; }
	/*Set variables*/
	timer = 0;
	int i = 0;


	/*Set manager*/
	mngr = (manager*)malloc(sizeof(manager));
	mngr->requests_in_time = (request**)malloc(24 * sizeof(request*));
	
	/*this order is necessary:*/
	inputAnalayze(buildForResources, "resources.txt", argv);
	inputAnalayze(buildForRepairs, "repairs.txt", argv);
	inputAnalayze(buildForRequests, "requests.txt", argv);
	
	/*Init mutex_timer*/
	sem_init(&mutex_timer, 0, 1);

	/*Set array of threads in size of all requests that will come*/
	pthread_t* RequestsThreads = (pthread_t*)malloc(sizeof(pthread_t)*mngr->numOfRequests);
	
	/*Create timerThread for menage time in this program*/
	pthread_t timerThread;
	pthread_create(&timerThread, NULL, (void*)timerManage, NULL);//

	/*Create all threads for request in appropriate time (according arrival time)*/
	while (i<mngr->numOfRequests)
	{
		sem_wait(&mutex_timer);
		/*Critical code section starts*/
		if (mngr->requests_in_time[timer] != NULL&&mngr->requests_in_time[timer]->arrive == FALSE) 
		{
			mngr->requests_in_time[timer]->arrive = TRUE;
			request* p = mngr->requests_in_time[timer];
			while (p != NULL)
			{
				pthread_create(&RequestsThreads[i], NULL, (void*)RequestInTreat, p);
				p = p->next;
				i++;
			}
		}
		/*Critical code section Ends*/
		sem_post(&mutex_timer);
	}
	/*Main thread waits for all requests threads will finishes*/
	for (i = 0; i<mngr->numOfRequests; i++)
		pthread_join(RequestsThreads[i], NULL);
	return 0;
}
void timerManage() {
	int day = 1;
	printf("Day %d:\n", day);
	while (1) {
		sleep(1);
		sem_wait(&mutex_timer);
		/*Critical code section starts*/
		timer++;
		if (timer == 24)
		{
			timer = 0;
			printf("~~~Day %d:~~~\n", ++day);
		}
		/*Critical code section Ends*/
		sem_post(&mutex_timer);
	}
}
void RequestInTreat(void* argv) 
{
	request* requestData = (request*)argv;
	printf("Car: %s - time: %d - arrived \n", requestData->carID, requestData->arrival_time);
	/*Set array of threads in size of all repairs that this car need**/
	pthread_t* repairsPerRequestThreads = (pthread_t*)malloc(sizeof(pthread_t)*requestData->num_of_repairs);
	
	/*Set array of repairs per current car/request, in size of all repairs that this car need*/
	cir* carInRepair = (cir*)malloc(sizeof(cir)*requestData->num_of_repairs);
	
	int i = 0;

	/*Set for each repair in request the relevant data, 
	that will sended to RepairInTreat func for appropriate thread*/
	for (i = 0; i < requestData->num_of_repairs; i++) {
		carInRepair[i].currentRequest = requestData;
		carInRepair[i].currentRepair = requestData->list_of_repairs[i];
		carInRepair[i].repairCompleted = FALSE;
	}

	/*Create all threads for repairs for this car*/
	for (i = 0; i < requestData->num_of_repairs; i++)
		pthread_create(&repairsPerRequestThreads[i], NULL,(void*)RepairInTreat, &carInRepair[i]);
	
	/*Request thread waits for all repairs (of this request) threads will finishes*/
	for (i = 0; i<requestData->num_of_repairs; i++)
		pthread_join(repairsPerRequestThreads[i], NULL);
	
	sem_wait(&mutex_timer);
	/*Critical code section starts*/
	printf("Car: %s - time: %d - service complete \n", requestData->carID, timer);
	/*Critical code section Ends*/
	sem_post(&mutex_timer);
}
void RepairInTreat(void* argv) {
	cir* carInRepairData = (cir*)argv;

	/*Repair try to catch appropriate resources and fix repair until it will succes-
	i.e until repairCompleted will come to be TRUE*/
	while (carInRepairData->repairCompleted == FALSE) {
		int n = 0;
		sem_wait(&(carInRepairData->currentRequest->sem_CarNotInRepair));
		/*Now just one repair thread can try to do, rest repairs threads wait*/
		while (n < carInRepairData->currentRepair->num_of_resources)
		{
			sem_wait(&carInRepairData->currentRepair->list_of_resources[n]->mutex_AvailableVsQuantity);
			/*Critical code section starts*/
			if (carInRepairData->currentRepair->list_of_resources[n]->available == TRUE)
			{
				carInRepairData->currentRepair->list_of_resources[n]->quantity--;
				if (carInRepairData->currentRepair->list_of_resources[n]->quantity == 0)
					carInRepairData->currentRepair->list_of_resources[n]->available = FALSE;
				/*Critical code section Ends (if condition true)*/
				sem_post(&carInRepairData->currentRepair->list_of_resources[n]->mutex_AvailableVsQuantity);

			}
			else {
				/*Critical code section Ends (if condition false)*/
				sem_post(&carInRepairData->currentRepair->list_of_resources[n]->mutex_AvailableVsQuantity);
				break;
				/*if there is at list one unavailable resorce- loop stopping and thread 
				let the another threads try... its our way to privent deadlocks*/
			}
			n++;
		}
		if (n == carInRepairData->currentRepair->num_of_resources)
		{
			printf("Car: %s - time: %d - All resources available for (%d) %s repair: ", carInRepairData->currentRequest->carID, timer, carInRepairData->currentRepair->id, carInRepairData->currentRepair->name);
			int i = 0;
			for (i = 0; i < n; i++)
				printf("%d) %s ", i + 1, carInRepairData->currentRepair->list_of_resources[i]->name);
			printf("\n");
			sem_wait(&mutex_timer);
			/*Critical code section starts*/
			printf("Car: %s - time: %d - repair: %s started\n", carInRepairData->currentRequest->carID, timer, carInRepairData->currentRepair->name);
			/*Critical code section Ends*/
			sem_post(&mutex_timer);
			/*Repair in progress now*/
			sleep(carInRepairData->currentRepair->duration);
			sem_wait(&mutex_timer);
			/*Critical code section starts*/
			printf("Car: %s - time: %d - repair: %s finished\n", carInRepairData->currentRequest->carID, timer, carInRepairData->currentRepair->name);
			/*Critical code section Ends*/
			sem_post(&mutex_timer);
			carInRepairData->repairCompleted = TRUE;
		}
		while (n > 0)/*We want return/free all resources*/
		{
			n--;
			sem_wait(&carInRepairData->currentRepair->list_of_resources[n]->mutex_AvailableVsQuantity);
			/*Critical code section starts*/
			carInRepairData->currentRepair->list_of_resources[n]->quantity++;
			if (carInRepairData->currentRepair->list_of_resources[n]->available == FALSE)
				carInRepairData->currentRepair->list_of_resources[n]->available = TRUE;
			/*Critical code section Ends*/
			sem_post(&carInRepairData->currentRepair->list_of_resources[n]->mutex_AvailableVsQuantity);
		}
		sem_post(&(carInRepairData->currentRequest->sem_CarNotInRepair));
		/*Now another one repair thread can try to do the mission*/

	}
}
void inputAnalayze(void(*build_func)(char*), char* name, char** argv) {
	int i;
	for (i = 1; i < 4; i++)
		if (strcmp(argv[i], name) == 0)
			build_func(argv[i]);
}
void buildForResources(char* file)
{
	int fd = open(file, O_RDONLY);
	if (fd == -1) {
		perror("open");
		exit(0);
	}
	char buffer[2] = { 0 };
	int read_bytes1;
	mngr->headResources = (resource*)malloc(sizeof(resource));
	if (mngr->headResources == NULL)
	{
		perror("memory");
		exit(0);
	}
	mngr->headResources->prev = NULL;
	mngr->headResources->next = NULL;
	mngr->headResources->id = 0;
	mngr->headResources->quantity = 0;
	resource* p = mngr->headResources;
	resource* pPrev;

	char tempName[100];
	int field = 1; //to know what the num of field we handle now
	int i = 1;
	memset(tempName, '\0', sizeof tempName);

	while (read(fd, (&buffer), 1) > 0)
	{
		if (buffer[0] == '\n')
			break;
		if (p == NULL)
		{
			p = (resource*)malloc(sizeof(resource));
			if (p == NULL) {
				perror("memory");
				exit(0);
			}
			p->next = NULL;
			p->id = 0;
			p->quantity = 0;
			p->prev = pPrev;
			pPrev->next = p;
		}
		while (buffer[0] != '\n')
		{
			while (buffer[0] != '\t')
			{
				switch (field)
				{
				case 1:
					p->id = p->id*i + atoi(buffer);
					i = i * 10;
					break;
				case 2:
					tempName[i - 1] = buffer[0];
					i++;
					break;
				case 3:
					p->quantity = p->quantity*i + atoi(buffer);
					i = i * 10;
					break;
				default:
					break;
				}
				memset(buffer, '\0', sizeof buffer);
				read_bytes1 = read(fd, (&buffer), 1);

				/*in case the file will end*/
				if (read_bytes1 <= 0)
					break;
				/*in case the row will end*/
				if (buffer[0] == '\n')
					break;
			}
			switch (field) {
			case 2:
				p->name = (char*)malloc(sizeof(char)*(strlen(tempName) + 1));
				if (p->name == NULL)
				{
					perror("memory");
					exit(0);
				}
				memset(p->name, '\0', sizeof (strlen(tempName) + 1));
				strcpy(p->name, tempName);
				memset(tempName, '\0', sizeof tempName);
				break;
			case 3:
				p->available = p->quantity > 0 ? TRUE : FALSE;
				sem_init(&p->mutex_AvailableVsQuantity, 0, 1);
			default: break;
			}
			field++;
			i = 1;
			/*in case the file will end*/
			if (read_bytes1 <= 0)
				break;
			/*if we come here not beacause we meet: \n,
			so we want to continue read this row*/
			if (buffer[0] != '\n')
				read_bytes1 = read(fd, (&buffer), 1);
		}
		mngr->numOfDiffrentReresources++;
		field = 1;//reset field for new row
		pPrev = p;
		p = p->next;
	}
	close(fd);
}
void buildForRepairs(char* file)
{
	int fd = open(file, O_RDONLY);
	if (fd == -1) {
		perror("open");
		exit(0);
	}
	char buffer[2] = { 0 };
	int read_bytes1;
	mngr->headRepairs = (repair*)malloc(sizeof(repair));
	if (mngr->headRepairs == NULL)
	{
		perror("memory");
		exit(0);
	}
	mngr->headRepairs->prev = NULL;
	mngr->headRepairs->next = NULL;
	mngr->headRepairs->id = 0;
	mngr->headRepairs->duration = 0;
	mngr->headRepairs->num_of_resources = 0;
	repair* p = mngr->headRepairs;
	repair* pPrev;
	int* resources_list;
	char tempName[100];
	int field = 1; //to know what the num of field we handle now
	int i = 1;
	memset(tempName, '\0', sizeof tempName);
	int j = 0;
	while (read(fd, (&buffer), 1) > 0)
	{
		if (buffer[0] == '\n')
			break;
		if (p == NULL)
		{
			p = (repair*)malloc(sizeof(repair));
			if (p == NULL) {
				perror("memory");
				exit(0);
			}
			p->next = NULL;
			p->id = 0;
			p->duration = 0;
			p->num_of_resources = 0;
			p->prev = pPrev;
			pPrev->next = p;
		}
		while (buffer[0] != '\n')
		{
			while (buffer[0] != '\t')
			{
				switch (field)
				{
				case 1:
					p->id = p->id*i + atoi(buffer);
					i = i * 10;
					break;
				case 2:
					tempName[i - 1] = buffer[0];
					i++;
					break;
				case 3:
					p->duration = p->duration*i + atoi(buffer);
					i = i * 10;
					break;
				case 4:
					p->num_of_resources = p->num_of_resources*i + atoi(buffer);
					i = i * 10;
					break;
				default:
					resources_list[j] = resources_list[j] * i + atoi(buffer);
					i = i * 10;
					break;

				}
				memset(buffer, '\0', sizeof buffer);
				read_bytes1 = read(fd, (&buffer), 1);

				/*in case the file will end*/
				if (read_bytes1 <= 0)
					break;
				/*in case the row will end*/
				if (buffer[0] == '\n')
					break;
			}
			switch (field)
			{
			case 1: break;
			case 2:
				p->name = (char*)malloc(sizeof(char)*(strlen(tempName) + 1));
				if (p->name == NULL)
				{
					perror("memory");
					exit(0);
				}
				memset(p->name, '\0', sizeof (strlen(tempName) + 1));
				strcpy(p->name, tempName);
				memset(tempName, '\0', sizeof tempName);
				break;
			case 3: break;
			case 4:
				p->list_of_resources = (resource**)malloc(p->num_of_resources * sizeof(resource*));
				if (p->list_of_resources == NULL)
				{
					perror("memory");
					exit(0);
				}
				resources_list = (int*)malloc(p->num_of_resources * sizeof(int));
				int k;
				for (k = 0; k < p->num_of_resources; k++)
					resources_list[k] = 0;
				break;
			default://case that field bigger then 4
				p->list_of_resources[j] = search_resource(mngr->headResources, resources_list[j]);
				j++;
				break;
			}
			field++;
			i = 1;
			/*in case the file will end*/
			if (read_bytes1 <= 0)
				break;
			/*if we come here not beacause we meet: \n,
			so we want to continue read this row*/
			if (buffer[0] != '\n')
				read_bytes1 = read(fd, (&buffer), 1);
		}
		mngr->numOfRepairs++;
		free(resources_list);
		j = 0;
		field = 1;//reset field for new row
		pPrev = p;
		p = p->next;
	}
	close(fd);
}
resource* search_resource(resource* head, int id)
{
	resource* current = head;  // Initialize current 
	while (current != NULL)
	{
		if (current->id == id)
			return current;
		current = current->next;
	}
	printf("Not found");
	return NULL;
}
void buildForRequests(char* file)
{
	int fd = open(file, O_RDONLY);
	if (fd == -1) {
		perror("open");
		exit(0);
	}
	char buffer[2] = { 0 };
	int read_bytes1;
	int* repair_list;
	int field = 1; //to know what the num of field we handle now
	int i = 1, j = 0;
	while (read(fd, (&buffer), 1) > 0)
	{
		if (buffer[0] == '\n')
			break;
		request* p;
		p = (request*)malloc(sizeof(request));
		if (p == NULL) {
			perror("memory");
			exit(0);
		}
		p->next = NULL;
		p->arrival_time = 0;
		p->num_of_repairs = 0;
		p->prev = NULL;
		while (buffer[0] != '\n')
		{
			while (buffer[0] != '\t')
			{
				switch (field)
				{
				case 1:
					p->carID[i - 1] = buffer[0];
					i++;
					break;
				case 2:
					p->arrival_time = p->arrival_time*i + atoi(buffer);
					i = i * 10;
					break;
				case 3:
					p->num_of_repairs = p->num_of_repairs*i + atoi(buffer);
					i = i * 10;
					break;
				default:
					repair_list[j] = repair_list[j] * i + atoi(buffer);
					i = i * 10;
					break;
				}
				memset(buffer, '\0', sizeof buffer);
				read_bytes1 = read(fd, (&buffer), 1);
				if (read_bytes1 <= 0)
					break;
				if (buffer[0] == '\n')
					break;
			}
			if (field == 3)
			{
				p->list_of_repairs = (repair**)malloc(p->num_of_repairs * sizeof(repair*));
				if (p->list_of_repairs == NULL)
				{
					perror("memory");
					exit(0);
				}
				repair_list = (int*)malloc(p->num_of_repairs * sizeof(int));
				if (repair_list == NULL)
				{
					perror("memory");
					exit(0);
				}
				int k;
				for (k = 0; k < p->num_of_repairs; k++)
					repair_list[k] = 0;
			}
			if (field > 3)
			{
				p->list_of_repairs[j] = search_repairs(mngr->headRepairs, repair_list[j]);
				j++;
			}
			field++;
			i = 1;
			if (read_bytes1 <= 0)
				break;
			if (buffer[0] != '\n')
				read_bytes1 = read(fd, (&buffer), 1);
		}
		p->arrive = FALSE;
		sem_init(&p->sem_CarNotInRepair, 0, 1);
		add_to_requests_in_time(&mngr->requests_in_time[p->arrival_time], p);
		free(repair_list);
		j = 0;
		field = 1;
	}
	close(fd);
}
void add_to_requests_in_time(request** head, request* current) {
	if (*head == NULL)
		*head = current;
	else {
		request* p = *head;
		while ((p)->next != NULL)
			(p) = (p)->next;
		(p)->next = current;
		(current)->prev = p;
	}
	mngr->numOfRequests++;
}
repair* search_repairs(repair* head, int id) {
	repair* current = head;  // Initialize current 
	while (current != NULL)
	{
		if (current->id == id)
			return current;
		current = current->next;
	}
	printf("Not found repair");
	return NULL;
}
/*
void printAllStructs() {
	int k;
	resource* p = mngr->headResources;
	while (p != NULL) {
		printf("%d %s %d\n", p->id, p->name, p->quantity);
		p = p->next;
	}
	printf("\n");
	repair* r = mngr->headRepairs;
	while (r != NULL) {
		printf("%d %s %d %d ", r->id, r->name, r->duration, r->num_of_resources);
		int k;
		for (k = 0; k < r->num_of_resources; k++)
			printf("%d ", r->list_of_resources[k]->id);
		printf("\n");
		r = r->next;
	}

	for (k = 0; k < 24; k++)
	{
		printf("\ntime: %d cars:", k);
		if (mngr->requests_in_time[k] != NULL)
		{
			request* p = mngr->requests_in_time[k];
			while (p != NULL)
			{
				printf("%s, ", p->carID);
				p = p->next;
			}
		}
		else
			printf("Empty");
	}
}
*/
